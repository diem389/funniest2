from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name="funniest",
      version='0.2',
      description='The funniest joke in the world',
      long_description=readme(),
      url="https://bitbucket.org/diem389/funniest2",
      author="Jaeyoon Jeong",
      license='MIT',
      packages=['funniest'],
      install_requires=['markdown'],
      zip_safe=False)
